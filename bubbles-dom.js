/*
 *  Bubbles Plugin
 *
 *  Made by Nick Jonas
 *  Under MIT License
 *
 *  Dependencies: jQuery
 */
(function ($, window, document, undefined) {

    // Create the defaults once
    var pluginName = "bubbles",
        defaults = {
            bubbleCount: 20,
            minDiam: 60,
            maxDiam: 140,
            blur: false,
            widthMod: 0,
            heightMod: 40,
            backgroundImages: []
        };

    // The actual plugin constructor
    function Plugin(element, options) {
        this.element = element;
        this.settings = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.features = [];
        this.measurements = {
            x: null,
            y: null,
            z: null,
            alpha: null,
            beta: null,
            gamma: null
        };
        this.init();
        $(element).data('isPlaying', false);

    }


    Plugin.prototype = {

        init: function () {
            this.createBubbles(this.settings.widthMod, this.settings.heightMod);
        },

        map_range: function (value, low1, high1, low2, high2) {
            return low2 + (high2 - low2) * (value - low1) / (high1 - low1);
        },

        // updates the position of a bubble element
        updatePosition: function ($elem, x, y, z) {
            var value = 'translate3d(' + x + 'px, ' + y + 'px, '+ z +'px)';
            value = Modernizr._prefixes.join('transform' + ':' + value + '; ');
            value = value.split(';');
            var props = {};
            for (var i = 0; i < value.length; i++) {
                var thisVal = value[i].split(':');
                if (thisVal.length === 2) {
                    props[thisVal[0]] = thisVal[1];
                }
            }
            $elem.css(props);
        },

        // create bubbles
        createBubbles: function (widthMod, heightMod, backgroundImages) {
            var self = this,
                el = this.element,
                width = $(el).width() + widthMod,
                height = $(el).height() + heightMod,
                docFrag = document.createDocumentFragment(),
                bubbles = [];

            if (this.settings.backgroundImages.length == 0) {
                length = this.settings.bubbleCount
            } else {
                length = this.settings.backgroundImages.length
            }

            console.log(length);

            for (var i = 0; i < length; i++) {
                // create div element
                var bubble = document.createElement('div');

                bubble.className += 'bubble';
                // create random properties
                var diam = (Math.random() * (this.settings.maxDiam - this.settings.minDiam)) + this.settings.minDiam,
                    x = Math.floor(Math.random() * width),
                    //x = width + (diam / 2) + Math.random(),
                    y = height - (diam / 2) + Math.random() * 700 - 600,
                    z = 400,
                    speed = (Math.random() / 4) + 0.5,
                    amplitude = (Math.random() * 300) + 45,
                    isOutline = false;

                var bubbleData = {
                    elem: bubble,
                    startX: x,
                    x: x,
                    y: y,
                    z: z,
                    radius: diam / 2,
                    speed: speed,
                    opacity: 1,
                    amplitude: amplitude,
                    isOutline: isOutline
                };
                bubbles.push(bubbleData);
                bubble.style.opacity = bubbleData.opacity;
                if (this.settings.backgroundImages.length == 0) {
                    $(bubble).css('background', "#000")
                } else {
                    $(bubble).css('background', 'url(' + this.settings.backgroundImages[i] + ')');
                    console.log(this.settings.backgroundImages[i]);
                }
                $(bubble).css('width', diam / 10);
                $(bubble).css('height', diam / 10);
                docFrag.appendChild(bubble);
            }

            el.appendChild(docFrag);

            var count = 0;

            function animate() {

                count++;

                for (var i = 0; i < bubbles.length; i++) {



                    var b = bubbles[i];
                
                    // if reached top, send back to bottom
                    if (b.y <= b.radius * -2) {
                        b.y = window.innerHeight - b.radius;
                    }
                    //if (b.x <= window.innerWidth) {
                    b.y = b.y - b.speed ;
                    if (i < 100) {
                        b.x = b.startX + (Math.sin(count / b.amplitude) * 1500) * b.speed;
                    } else {
                        b.x = b.startX - (Math.sin(count / b.amplitude) * 1500) * b.speed;
                    }
                    if (b.x > window.innerWidth || b.x < b.radius * -2) {
                        b.startX = b.x;
                        b.speed = -b.speed;
                    }
                    b.z = b.z ;
                    //                    }
                    self.updatePosition($(b.elem), b.x, b.y, b.z);
                    //console.log(b.radius * -2 + 1200);

                }
            }


            // shim layer with setTimeout fallback
            window.requestAnimFrame = (function () {
                return window.requestAnimationFrame ||
                    window.webkitRequestAnimationFrame ||
                    window.mozRequestAnimationFrame ||
                    function (callback) {
                        window.setTimeout(callback, 1000 / 60);
                    };
            })();
            (function animloop() {
                requestAnimFrame(animloop); 
                animate();
            })();

        }

    };


    $.fn.startBubbleAnimation = function (options) {
        $(this).each(function (i) {
            var $this = $(this);
            $this.data('isPlaying', true);
        });
    };

    $.fn.stopBubbleAnimation = function (options) {
        $(this).each(function (i) {
            var $this = $(this);
            $this.data('isPlaying', false);
        });
    };

    // A really lightweight plugin wrapper around the constructor,
    // preventing against multiple instantiations
    $.fn[pluginName] = function (options) {
        return this.each(function () {
            if (!$.data(this, "plugin_" + pluginName)) {
                $.data(this, "plugin_" + pluginName, new Plugin(this, options));
            }
        });
    };

})(jQuery, window, document);